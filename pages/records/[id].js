import RecordDetails from '../../components/RecordDetails'
import { useRouter } from 'next/router'
import { Spinner } from 'react-bootstrap'


export default function record({ recordData }) {
	const router = useRouter()

	if(router.isFallback){
		return <> <span> Fetching Record Details...</span>
		<Spinner animation="border" variant="primary"/></>
	}else{
		return <RecordDetails recordData={recordData}/>
	}

}


export async function getStaticPaths(){
	const res = await fetch('http://localhost:4000/api/records')
	const data = await res.json()
	const paths = data.map(datum =>{
		return{
			params:{
				id: datum._id
			}
		}
	})

	return {
		paths,
		fallback: true
	}
}

export async function getStaticProps({ params }){
	const res = await fetch(`http://localhost:4000/api/records/${params.id}`)
	const recordData = await res.json()
	return {
		props: {
			recordData
		}
	}
}