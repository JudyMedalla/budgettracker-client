import { useState, useEffect, useContext} from 'react'
import { Form, Row, Col, Jumbotron, Card, Button, CardGroup, Modal, InputGroup} from 'react-bootstrap'
import RecordCard from '../../components/RecordCard'
import AddNewRecord from '../../components/AddNewRecord'
import CurrentBalance from '../../components/CurrentBalance'
import LogoImage from '../../components/images/logo.jpg'
import UserContext from '../../UserContext'




export default function Records({recordData, data}){

  const { user } = useContext(UserContext)
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);



  return(
    
    <div>
      <Row>
        <Col xs={12} ms={5} lg={5} className="mx-auto"> 
          <CurrentBalance/>
            <Button className="mt-5 rounded-pill" variant="info" onClick={handleShow} block> Add New Record
              </Button>
              
              <Modal
                  dialogClassName="modal-90w"
                  show={show}
                  onHide={handleClose}
                  backdrop="static"
                  keyboard={false}
                  size="lg"
                  aria-labelledby="example-custom-modal-styling-title"
                  >
                <Modal.Header closeButton>
                </Modal.Header>
                <Modal.Body>
                   <AddNewRecord/>
                </Modal.Body>
              </Modal>

         </Col>

         <Col xs={12} ms={7} lg={7} className="mx-auto mt-4"> 
            <h1>Records</h1>
              {(data.length === 0)
                ? (
                	<Jumbotron id="card">
                      <h1>There are no available Records yet</h1>
                  	</Jumbotron>
                  )
                : 
                  (
                    data.map(item => <RecordCard recordData={item}/>)
					)
               } 
          </Col>   
      </Row>
    </div>

    )
}



export async function getServerSideProps() {
  const res = await fetch('http://localhost:4000/api/records')
  const data = await res.json()

  return {
    props:{
      data
    }
  }
}