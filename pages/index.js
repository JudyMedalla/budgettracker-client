import { useState, useEffect, useContext } from 'react'
import { Form, Button, Container, Row, Col, Modal } from 'react-bootstrap'
import UserContext from '../UserContext'
import Router from 'next/router'
import LoginImage from '../components/images/pic.png'
import LogoImage from '../components/images/logo.jpg'
import Link from 'next/link'
import { GoogleLogin } from 'react-google-login'
import Register from '../components/register'



export default function home() {

  const { setUser } = useContext(UserContext)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  //Google Login
    const captureLoginResponse = (response) => {
        console.log(response)
        const payload = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({tokenId: response.tokenId})
        }
        fetch(`http://localhost:4000/api/users/verify-google-id-token`, payload)
        .then((response) => response.json())
        .then(data => {
            //if the data return by the api has an accesstoken, we'll set the localStorage
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                setUser({id: data._id, isAdmin: data.isAdmin})
                Router.push('/records')

            }else {
            //else, if the data return by the api is an error msgs
            if(data.error == 'google-auth-error') {
                alert("Error! Google authentication procedure failed.")
                //error == 'google auth error', return a msg
            }else if(data.error === 'login-type-error'){
                alert("Error! You may have registered through a different login procedure.")
                //error == 'login type error', return a msg
            }


            }
        })
    }



 function authenticate(e){
        //prevent redirection via form submission 
        e.preventDefault()

        fetch(`http://localhost:4000/api/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data.accessToken){
                localStorage.setItem('token', data.accessToken)
                fetch(`http://localhost:4000/api/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}` 
                    }
                })
                .then(res => res.json())
                .then(data => {
                    //set the global user state to have the proper id and isAdmin properties
                    setUser({
                        id: data._id,
                        isActive: data.isActive
                    })
                    Router.push('/records')
                })
            }else{
                // Router.push('/error')
            }
        })
    }

  return (
      <>
   <Container>
     <Row className='max-w-screen-xl m-0 sm:m-20 bg-white shadow sm:rounded-lg flex justify-center flex-1 mt-5 mb-5'>
      
        <Col xs={12} ms={5} lg={5} className="mb-5 pt-2">
        <div className='text-2xl xl:text-3xl mt-0 text-center'>
          <img src={LogoImage} alt='logo' id='LogoImage'/>
        </div>
              <h1 className='text-2xl xl:text-3xl font-extrabold mx-auto text-center'> Login </h1>      
                  <Form 
                  className='w-full flex-1 mt-8 text-indigo-500 mt-5 ml-4 mr-2'
                  onSubmit={(e) => authenticate(e)}>
                    <Form.Group className="mx-auto max-w-xs relative mt-5">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" value={email} onChange={(e)=>setEmail(e.target.value)} 
                         className='email w-full px-8 py-4 font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white rounded-pill'
                         placeholder="email"/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={password} onChange={(e)=>setPassword(e.target.value)} 
                         className='password w-full px-8 py-4 font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white rounded-pill'
                         placeholder="password"/>
                    </Form.Group>
                    <div className='my-12 border-b text-center'>
                    <Button className='mt-3 mb-3 font-semibold bg-indigo-500 text-gray-100 rounded-pill hover:bg-indigo-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none' type="submit" block>Log In</Button>
                    </div>

                    <div className='my-12 border-b text-center'>
                      <GoogleLogin  
                          clientId='35719299842-9latndrido6hrbsqbf5h064b32887up3.apps.googleusercontent.com' 
                          render={renderProps => (
                              <Button variant="danger" className='mt-3 inline-block font-semibold bg-indigo-500 text-gray-100 rounded-pill hover:bg-indigo-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none' onClick={renderProps.onClick} disabled={renderProps.disabled} block>Login using Google Account</Button>
                          )}       
                          onSuccess={captureLoginResponse} 
                          onFailure={captureLoginResponse} 
                          cookiePolicy={'single_host_origin'}/>
                    </div>

                   
                        <Form.Group>
                          <p className="mt-3 mb-1">No account yet? 
                          </p>
                            
                              <Button variant="outline-primary" className='mt-3 mb-3 font-semibold bg-indigo-500 text-gray-100 rounded-pill hover:bg-indigo-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none' block  onClick={handleShow}>
                                <a>Sign up here</a>
                              </Button>
                               <Modal
                                  dialogClassName="modal-90w"
                                  show={show}
                                  onHide={handleClose}
                                  backdrop="static"
                                  keyboard={false}
                                  size="xl"
                                  aria-labelledby="example-custom-modal-styling-title"
                                >
                                  <Modal.Header closeButton>
                                      <Modal.Title>Sign Up</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                      <Register/>
                                    </Modal.Body>
                                  </Modal>
                            
                        </Form.Group>
                  </Form>


          </Col>
          <Col xs={12} ms={7} lg={7} className="mb-2 mt-5 pt-5">
            <div className='flex-1 bg-indigo-100 text-center hidden lg:flex'>
              <div className='m-12 xl:m-16 w-full bg-contain bg-center bg-no-repeat'>
                <img src={LoginImage} alt='login' id='LoginImage' className="image-fluid"/>
              </div>
            </div>
          </Col>
    
    </Row>
 </Container>
    

      </>
  )
}

