import { useState, useEffect, useContext} from 'react'
import { Form, Row, Col, Jumbotron, Card, Button, CardGroup, Modal} from 'react-bootstrap'
import UserContext from '../../UserContext'
import CategoryCard from '../../components/CategoryCard'
import AddNewCategory from '../../components/AddNewCategory'

export default function categories({datum}){

	const { user } = useContext(UserContext)
    const [categoryName, setCategoryName] =useState('')
    const [categoryType, setCategoryType] =useState('')
    const [isActive, setIsActive] =useState('')
    const [notify, setNotify] = useState(false)
    const [activeCategories, setActiveCategories] = useState([])





    useEffect(()=>{
        if(categoryName.length < 80) {
            setIsActive(true)
        }else {
            setIsActive(false)
        }
    }, [categoryName])



	 	function createCategory(e){
	        e.preventDefault()

	        fetch('http://localhost:4000/api/categories',{
	            method: 'POST',
	            headers: {
	                'Content-Type': 'application/json',
	                'Authorization': `Bearer ${localStorage.getItem('token')}`
	            },
	            body: JSON.stringify({
	                categoryName: categoryName,
	                categoryType: categoryType,
	            })
	        })
	        .then(res => res.json())
	        .then(data => {
	            if(data===true){
	                Router.push('/categories')
	            }else{
	                setNotify(true)
	            }
	        })
	    }



	return(

		<div>
	      <Row>
	        <Col xs={12} ms={5} lg={5} className="mt-5 pt-5"> 
	        	<div className="mt-5">
				<div className='mx-auto max-w-screen-xl m-0 sm:m-20 bg-white shadow sm:rounded-lg flex justify-center flex-1 mt-1 mb-1' id="card">
				       <AddNewCategory/>
				</div>
				</div>
	        </Col>

	        <Col xs={12} ms={7} lg={7} className="mx-auto mt-4"> 
	            <h1>Categories </h1>
				 {(datum.length === 0)
                ? (
                	<Jumbotron id="card">
                      <h1>There are no available Records yet</h1>
                  	</Jumbotron>
                  )
                : 
                  (
                    datum.map(item => <CategoryCard categoryData={item}/>)
					)
	        }
	        </Col>   
	      </Row>
	    </div>


		)
}


export async function getServerSideProps() {
  const res = await fetch('http://localhost:4000/api/categories')
  const datum = await res.json()

  return {
    props:{
      datum
    }
  }
}