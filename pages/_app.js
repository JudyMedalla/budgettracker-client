import { useState, useEffect } from 'react'
import {UserProvider} from '../UserContext'
import Head from 'next/head'
import '../styles/index.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container } from 'react-bootstrap'
import NavBar from '../components/NavBar'

function MyApp({ Component, pageProps }) {

	 const [user, setUser] = useState({
    id: null,
    isActive: null
  })

 
  const unsetUser = () => {
    localStorage.clear()
    setUser({
      id: null,
      isActive: null
    })
  }


  useEffect(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })

    .then(res => res.json())
    .then(data => {
      console.log(data)
     
      if(data._id){
        setUser({
          id: data._id,
        })
      }else{
        setUser({
          id: null,
 
        })
      }      
    })
  }, [user.id])



  return(
<>
	<UserProvider value={{user, setUser, unsetUser}}>
    <NavBar />
    <Container>
	  	<Component {...pageProps} />
	  </Container>
	</UserProvider>
	</>
  	) 
}

export default MyApp
