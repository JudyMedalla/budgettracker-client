import { useState, useEffect, useContext } from 'react'
import { Card, CardGroup, Button, Form, Container, Row, Col, Alert } from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../UserContext'



export default function AddNewRecord(){

    const { user } = useContext(UserContext)
    const [name, setName] =useState('')
    const [type, setType] =useState('')
    const [amount, setAmount] =useState('')
    const [description, setDescription] =useState('')
    const [isActive, setIsActive] =useState('')
    const [notify, setNotify] = useState(false)


    useEffect(()=>{
        if(name.length < 80 && description.length < 200) {
            setIsActive(true)
        }else {
            setIsActive(false)
        }
    }, [name, description])


    function createRecord(e){
        e.preventDefault()

        fetch('http://localhost:4000/api/records/add-record',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                type: type,
                amount: amount,
                description: description
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data==true){
                Router.reload();
            }else{
                setNotify(true)
            }
        })
    }


    return(
        <>
        <Container>
        <div className="mx-auto">
        <Form
        onSubmit={(e)=> createRecord(e)} 
        className='w-full flex-1 text-indigo-500 ml-5 mr-5 mt-5 mb-5'>
        <h3 className="mt-5">Add New Record</h3>
                            <Form.Group className="mx-auto max-w-xs relative mt-2">
                                Category Name:
                                <Form.Control 
                                value={name}
                                onChange={e => setName(e.target.value)}
                                required
                                type="text"  
                                 className='email rounded-pill'
                                 placeholder=""/>
                                 {name.length >= 80 ? 
                                    <Alert variant="warning">
                                    Name has exceeded maximum length</Alert>:null}
                                 
                            </Form.Group> 
                            <Form.Group className="mx-auto max-w-xs relative">
                                Category Type:
                                <Form.Control 
                                value={type}
                                onChange={e => setType(e.target.value)}
                                required
                                className='email rounded-pill'
                                placeholder=""
                                as="select"  
                                className='email rounded-pill'
                                placeholder="Select Category">
                                
                                <option value="">Select Category</option>
                                <option value="Income">Income</option>
                                <option value="Expense">Expense</option>
                                </Form.Control>
                            </Form.Group>
                                 <Form.Group className="mx-auto max-w-xs relative">
                                Amount
                                <Form.Control
                                value={amount}
                                onChange={e => setAmount(e.target.value)}
                                required
                                 type="number"  
                                 className='email rounded-pill'
                                 placeholder=""/>
                            </Form.Group>
                            <Form.Group>
                                description
                                <Form.Control 
                                as="textarea" 
                                rows="3" 
                                value={description} 
                                onChange={e => setDescription(e.target.value)} 
                                required
                                 className='password rounded-pill'
                                 placeholder=""/>
                                 {description.length >= 200 ? <Alert variant="warning">Description has exceeded maximum length</Alert>:null}  
                            </Form.Group>
                            <div className='my-12 border-b text-center'>


                        {isActive===true
                        ? <Button type="submit" variant="info" className="rounded-pill" block>Add</Button>
                        : <Button disabled type="submit" variant="info" className="rounded-pill" block>Add</Button>}

        </div>
        </Form>

                    {notify === true
                    ? <Alert variant="danger">Failed to create a Record!</Alert>
                    : null}
                </div>
            </Container>
        </>
        )
}