import { useContext } from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap'
import UserContext from '../UserContext'
import DeleteButton from './DeleteButton'

export default function RecordCard({categoryData}){

   const { user } = useContext(UserContext)
  return(

<Card id="card" className="mt-3 max-w-screen-xl m-0 sm:m-20 bg-white shadow sm:rounded-lg flex justify-center flex-1">
  <Card.Body>

  <Row>

    <Col xs={8} ms={8} lg={8}>
        <Card.Title>{categoryData.categoryName}</Card.Title>
        <Card.Text>
          {categoryData.categoryType}
        </Card.Text>
    </Col>

    <Col xs={8} ms={8} lg={8}>
    <Card.Text key={categoryData._id}>
        <DeleteButton categoryId={categoryData._id}/>
    </Card.Text>
  </Col>

  </Row>
  </Card.Body>
</Card>
)
}