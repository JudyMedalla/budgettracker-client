import { Card, CardGroup } from 'react-bootstrap'


export default function CurrentBalance(){
	return(
		<div>
			     <h3 className="mt-5">Balance</h3>
         <h1>Php 0.0</h1>

      <CardGroup>
                  <Card id="cardleft" className="mt-3 max-w-screen-xl m-0 sm:m-20 bg-white shadow sm:rounded-lg flex justify-center flex-1">
                    <Card.Body>
                      <Card.Text className="text-center">
                        0.00
                      </Card.Text>  
                    </Card.Body>
                  </Card>

                  <Card id="cardright" className="mt-3 max-w-screen-xl m-0 sm:m-20 bg-white shadow sm:rounded-lg flex justify-center flex-1">
                    <Card.Body>
                      <Card.Text className="text-center">
                        0.00
                      </Card.Text>
                    </Card.Body>
                  </Card>
      </CardGroup>
		</div>
		)
}