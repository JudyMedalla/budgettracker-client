import { useState, useEffect, useContext } from 'react'
import { Form, Button, Container, Row, Col } from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../UserContext'
import RegisterImage from '../components/images/register.png'
import LogoImage from '../components/images/logo.jpg'
// import { GoogleLogin } from 'react-google-login'

export default function register(){


	const { setUser } = useContext(UserContext)
    
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [mobileNumber, setMobileNumber] = useState('')

 
    const [isActive, setIsActive] = useState(false)

    
    useEffect(() => {
       
        if((password1 !== '' && password2 !== '') && (password2 === password1) /* && (mobileNumber.length === 10)*/){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [password1, password2, /*mobileNumber*/])



    function registerUser(e){
        e.preventDefault()

        fetch(`http://localhost:4000/api/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password1: password1,
                password2: password2,
              
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log('if data', data ? true : false);
            console.log('response data', data);

            if (data) {
                console.log('pumapasok rito');
                Router.reload()
            }

        	// if(data === true){
        	// 	Router.push('/')
        	// }else{
        	// 	// Router.push('error');
        	// }
        })
    }
	return(
	<>
    <Container>
        <Row className='max-w-screen-xl m-0 sm:m-20 bg-white shadow sm:rounded-lg flex justify-center flex-1 mt-1 mb-1'>
            <Col xs={12} ms={5} lg={5} className="mb-1 pt-1"> 
                <Form 
                className='w-full flex-1 text-indigo-500 mt-0 ml-4'
                onSubmit={(e) => registerUser(e)}>
                    <Form.Group className="mx-auto max-w-xs relative mt-5">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" value={firstName} onChange={e => setFirstName(e.target.value)} className='fields w-full px-8 py-4 font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white rounded-pill'
                         placeholder="First Name" required></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control 
                        className=' fields w-full px-8 py-4 font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white rounded-pill'
                         placeholder="Last Name"
                        type="text" value={lastName} onChange={e => setLastName(e.target.value)} required></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} className='fields w-full px-8 py-4 font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white rounded-pill'
                         placeholder="Email" required></Form.Control>
                    </Form.Group>
                    {/*<Form.Group>
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control type="text" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)} placeholder="9xxxxxxxx" required></Form.Control>
                    </Form.Group>*/}
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={password1} onChange={e => setPassword1(e.target.value)} className='fields w-full px-8 py-4 font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white rounded-pill'
                         placeholder="Password" required></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control type="password" value={password2} onChange={e => setPassword2(e.target.value)} 
                        className='fields w-full px-8 py-4 font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white rounded-pill'
                         placeholder="Verify Password" required></Form.Control>
                    </Form.Group>
                    {isActive === true 
                    ? <Button className='mt-3 mb-3 font-semibold bg-indigo-500 text-gray-100 rounded-pill hover:bg-indigo-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none d-block' type="submit">Submit</Button> 
                    : <Button className='mt-3 mb-3 font-semibold bg-indigo-500 text-gray-100 rounded-pill hover:bg-indigo-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none d-block' type="submit" disabled>Submit</Button>}
        	    </Form>
            </Col>
            <Col xs={12} ms={7} lg={7} className="mb-5 pt-3 mt-5">
                <div className='flex-1 bg-indigo-100 text-center hidden lg:flex'>
                  <div className='m-12 xl:m-16 w-full bg-contain bg-center bg-no-repeat'>
                    <img src={RegisterImage} alt='login' id='LoginImage'/>
                  </div>
                </div>
            </Col>
        </Row>
    </Container>
    </>

		)
}